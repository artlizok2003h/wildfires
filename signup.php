<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Wildfires - Signup</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <link rel='stylesheet' href='css/bootstrap.min.css'>
  <link rel="stylesheet prefetch" href="css/signup_auth.css">

  
</head>

<body>
  <div class="container">
	<header>
		<h1>
			<a>
				<img src="images/reg.svg" alt="Authentic Collection">
			</a>
		</h1>
	</header>
	<h1 class="text-center">Регистрация</h1>
	<form method="POST" action="signup.php" class="registration-form">
		<label class="col-one-half">
			<span class="label-text">Введите логин</span>
			<input type="text" name="login">
		</label>
		<label class="col-one-half">
			<span class="label-text">Введите ваше имя</span>
			<input type="text" name="name">
		</label>
		<label>
			<span class="label-text">Введите ваш Email</span>
			<input type="text" name="email">
		</label>
		<label class="textarea">
			<span class="label-text">Выберите ваш регион:</span>
			<select id="selectID" name="region">
				<option>Не выбрано</option>
				<option>Республика Адыгея</option>
				<option>Республика Алтай</option>
				<option>Республика Башкортостан</option>
				<option>Республика Бурятия</option>
				<option>Республика Дагестан</option>
				<option>Республика Ингушетия</option>
				<option>Кабардино-Балкарская Республика</option>
				<option>Республика Калмыкия</option>
				<option>Карачаево-Черкесская Республика</option>
				<option>Республика Карелия</option>
				<option>Республика Коми</option>
				<option>Республика Крым</option>
				<option>Республика Марий Эл</option>
				<option>Республика Мордовия</option>
				<option>Республика Саха (Якутия)</option>
				<option>Республика Северная Осетия</option>
				<option>Республика Татарстан</option>
				<option>Республика Тыва</option>
				<option>Удмуртская Республика</option>
				<option>Республика Хакасия</option>
				<option>Чеченская Республика</option>
				<option>Чувашская Республика</option>
				<option>Алтайский край</option>
				<option>Забайкальский край</option>
				<option>Камчатский край</option>
				<option>Краснодарский край</option>
				<option>Красноярский край</option>
				<option>Пермский край</option>
				<option>Приморский край</option>
				<option>Ставропольский край</option>
				<option>Хабаровский край</option>
				<option>Амурская область</option>
				<option>Архангельская область</option>
				<option>Астраханская область</option>
				<option>Белгородская область</option>
				<option>Брянская область</option>
				<option>Владимирская область</option>
				<option>Волгоградская область</option>
				<option>Вологодская область</option>
				<option>Воронежская область</option>
				<option>Ивановская область</option>
				<option>Иркутская область</option>
				<option>Калининградская область</option>
				<option>Калужская область</option>
				<option>Кемеровская область</option>
				<option>Кировская область</option>
				<option>Костромская область</option>
				<option>Курганская область</option>
				<option>Курская область</option>
				<option>Ленинградская область</option>
				<option>Липецкая область</option>
				<option>Магаданская область</option>
				<option>Московская область</option>
				<option>Мурманская область</option>
				<option>Нижегородская область</option>
				<option>Новгородская область</option>
				<option>Новосибирская область</option>
				<option>Омская область</option>
				<option>Оренбургская область</option>
				<option>Орловская область</option>
				<option>Пензенская область</option>
				<option>Псковская область</option>
				<option>Ростовская область</option>
				<option>Рязанская область</option>
				<option>Самарская область</option>
				<option>Саратовская область</option>
				<option>Сахалинская область</option>
				<option>Свердловская область</option>
				<option>Смоленская область</option>
				<option>Тамбовская область</option>
				<option>Тверская область</option>
				<option>Томская область</option>
				<option>Тульская область</option>
				<option>Тюменская область</option>
				<option>Ульяновская область</option>
				<option>Челябинская область</option>
				<option>Ярославская область</option>
				<option>Город Москва</option>
				<option>Город Санкт-Петербург</option>
				<option>Еврейская АО</option>
				<option>Ненецкий АО</option>
				<option>Ханты-Мансийский АО</option>
				<option>Чукотский АО</option>
				<option>Ямало-Ненецкий АО</option>
			</select>
		</label>
		<label class="password">
			<span class="label-text">Придумайте пароль</span>
			<button class="toggle-visibility" title="toggle password visibility" tabindex="-1">
				<span class="glyphicon glyphicon-eye-close"></span>
			</button>
			<input type="password" name="password">
		</label>
		<div class="text-center">
			<button type="submit" class="submit" name="register">Зарегистрироваться</button>
		</div>
		<?php
require("connectdb.php");
require("session.php");

if (!empty($_POST)){
    $result = mysqli_query($connect, "SELECT * FROM users WHERE login=\"".$_POST['login']."\"");
    if(mysqli_num_rows($result) == 0){
        mysqli_query($connect, "INSERT INTO users (login, name, email, region, password) VALUES (
            \"".$_POST["login"]."\", 
            \"".$_POST["name"]."\",
            \"".$_POST["email"]."\",
			\"".$_POST["region"]."\",
            \"".md5($_POST["password"])."\"
            )"
        );
    }
	echo "<h4 style='text-align:center'>Регистрация прошла успешно!</h4>";
	header( "refresh:2; url=index.php" );
}
?>
	</form>
</div>
</body>

</html>

<script src="js/signup.js"></script>