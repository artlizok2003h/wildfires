<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Wildfires</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">

   
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body data-spy="scroll" data-target="#ftco-navbar" data-offset="200">
    
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
      <div class="container">
        <a class="navbar-brand" href="index.php">Статистика лесных пожаров</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
          <ul class="navbar-nav ml-auto">
          <?php
            require("connectdb.php");
            require("session.php");
            if($session_user) {
              $role = mysqli_query($connect, "SELECT role FROM users WHERE id=".$session_user['id']);
              $row = mysqli_fetch_array($role);
              if($row['role'] == 'user' || $row['role'] == 'admin') {
                echo "<li class='nav-item active'><a href='index.php' class='nav-link'>Главная</a></li>";
                echo "<li class='nav-item'><a href='regions.php' class='nav-link'>Субъекты</a></li>";
                echo "<li class='nav-item'><a href='districts.php' class='nav-link'>Округа</a></li>";
                echo "<li class='nav-item'><a href='feedback.php' class='nav-link'>Обратная связь</a></li>";
              }
          } 
          else {
            echo "<li class='nav-item active'><a href='index.php' class='nav-link'>Главная</a></li>";
            echo "<li class='nav-item'><a href='regions.php' class='nav-link'>Субъекты</a></li>";
            echo "<li class='nav-item'><a href='districts.php' class='nav-link'>Округа</a></li>";
            echo "<li class='nav-item'><a href='signup.php' class='nav-link'>Зарегистрироваться</a></li>";
            echo "<li class='nav-item'><a href='auth.php' class='nav-link'>Авторизоваться</a></li>";
          }
            ?>
          </ul>
        </div>
      </div>
    </nav>
    <!-- END nav -->

    <section class="ftco-cover ftco-slant" style="background-image: url(images/bg_fire.jpg);" id="section-home">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center ftco-vh-100">
          <div class="col-md-10">
            <h1 class="ftco-heading ftco-animate">Лесные пожары – стихийное бедствие для мира природы и для людей</h1>
            <p><a href=#about class="btn btn-primary ftco-animate">Начать работу с приложением</a></p>
          </div>
        </div>
      </div>
    </section>
    
    

    <section class="ftco-section bg-light  ftco-slant ftco-slant-white" id="about">
      <div class="container">
        
        <div class="row">
          <div class="col-md-12 text-center mb-5 ftco-animate">
            <h2 class="text-uppercase ftco-uppercase">Возможности приложения "Wildfires"</h2>
            <div class="row justify-content-center">
              <div class="col-md-7">
                <p class="lead">В нашем приложении представлены следующие функции:</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="media d-block mb-4 text-center ftco-media p-md-5 p-4 ftco-animate">
              <div class="media-body">
                <h5 class="mt-0">Статистика субъектов</h5>
                <p class="mb-5">При нажатии на элемент карты РФ пользователь получит информацию о количестве пожаров конкретного субъекта, а также может ознакомиться с рейтингами субъектов по пожарам в таблицах.</p>
                <p class="mb-0"><a href="regions.php" class="btn btn-primary btn-sm">Перейти к карте</a></p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="media d-block mb-4 text-center ftco-media p-md-5 p-4 ftco-animate">
              <div class="media-body">
                <h5 class="mt-0">Статистика округов</h5>
                <p class="mb-5">Аналогичная статистика также приведена на карте округов РФ и в таблице.</p>
                <p class="mb-0"><a href="districts.php" class="btn btn-primary btn-sm">Перейти к карте</a></p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="media d-block mb-4 text-center ftco-media p-md-5 p-4 ftco-animate">
              <div class="media-body">
                <h5 class="mt-0">Регистрация</h5>
                <p class="mb-5">Зарегистрируйтесь в приложении, чтобы иметь возможность оставить обратную связь и получать рассылку об актуальной ситуации с пожарами в вашем регионе.</p>
                <p class="mb-0"><a href="signup.php" class="btn btn-primary btn-sm">Зарегистрироваться</a></p>
              </div>
            </div>
          </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->

    <section class="ftco-section ftco-slant ftco-slant-light">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center ftco-animate">
            <h2 class="text-uppercase ftco-uppercase">Причины возникновения</h2>
            <div class="row justify-content-center mb-5">
              <div class="col-md-7">
                <p class="lead">Различают два вида причин: естественные (связанные с природными явлениями) и антропогенные (связанные с человеком).</p>
              </div>
            </div>
          </div>
        </div>
        <!-- END row -->
        <div class="row">
          <div class="col-md-12">
            <div class="owl-carousel ftco-owl">
              
              <div class="item ftco-animate">
                <div class="media d-block text-left ftco-media p-md-5 p-4">
                  <div class="media-body">
                    <p><h5>Курение в лесу</h5></p>
                  </div>
                </div>
              </div>

              <div class="item ftco-animate">
                <div class="media d-block text-left ftco-media p-md-5 p-4">
                  <div class="media-body">
                      <p><h5>Малое количество осадков и высокие температуры летом</h5></p>
                  </div>
                </div>
              </div>

              <div class="item ftco-animate">
                <div class="media d-block text-left ftco-media p-md-5 p-4">
                  <div class="media-body">
                    <p><h5>Походные костры, сжигание мусора и листьев</h5></p>
                  </div>
                </div>
              </div>

              <div class="item ftco-animate">
                <div class="media d-block text-left ftco-media p-md-5 p-4">
                  <div class="media-body">
                    <p><h5>Дорожно-транспортные происшествия</h5></p>
                  </div>
                </div>
              </div>

              <div class="item ftco-animate">
                <div class="media d-block text-left ftco-media p-md-5 p-4">
                  <div class="media-body">
                    <p><h5>Молнии, извержения вулканов</h5></p>
                  </div>
                </div>
              </div>

              <div class="item ftco-animate">
                <div class="media d-block text-left ftco-media p-md-5 p-4">
                  <div class="media-body">
                    <p><h5>Намеренные поджоги</h5></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    

    <section class="ftco-section ftco-slant ftco-slant-light" id="section-about">
      <div class="container">

        <div class="row mb-5">
          <div class="col-md-12 text-center ftco-animate">
            <h2 class="text-uppercase ftco-uppercase">Что делать при обнаружении возгорания</h2>
            <div class="row justify-content-center mb-5">
              <div class="col-md-7">
                <p class="lead">Правила поведения, если вы обнаружили пожар в лесу:</a></p>
              </div>
            </div>
          </div>
        </div>
        <!-- END row -->


        <div class="row no-gutters align-items-center ftco-animate">
          <div class="col-md-6 mb-md-0 mb-5">
            <img src="images/bg_3.jpg" width=500 class="img-fluid">
          </div>
          <div class="col-md-6 p-md-5">
            <h3 class="h3 mb-4">Сообщите в службу спасения</h3>
            <p>Если вы обнаружили в лесу пожар, немедленно сообщите об этом в службу спасения, в администрацию сельского округа или в лесничество. Запомните номер, на который следует звонить в случае лесного пожара: «112» (только для мобильных телефонов).</p>
          </div>
        </div>
        <div class="row no-gutters align-items-center ftco-animate">
          <div class="col-md-6 order-md-3 mb-md-0 mb-5">
            <img src="images/bg_1.jpg" width=500 class="img-fluid">
          </div>
          <div class="col-md-6 p-md-5 order-md-1">
            <h3 class="h3 mb-4">Тушение пожара</h3>
            <p>Если обнаруженный вами пожар ещё не набрал силу, примите меры по его тушению с помощью воды, земли, песка, веток лиственных деревьев, плотной одежды. Наиболее эффективный способ тушения лесного пожара – забрасывание кромки пожара землёй.

              При тушении очага лесного пожара не отходите далеко от дорог и просек, поддерживайте связь с остальными участниками тушения пожара с помощью зрительных и звуковых сигналов.
              
              Если огонь разгорелся слишком сильно, и вы не в силах его остановить, срочно покиньте место происшествия.</p>
          </div>
        </div>
        <div class="row no-gutters align-items-center ftco-animate">
          <div class="col-md-6 mb-md-0 mb-5">
            <img src="images/bg_2.jpg" width=500 class="img-fluid">
          </div>
          <div class="col-md-6 p-md-5">
            <h3 class="h3 mb-4">Передвижение во время пожара</h3>
            <p>При лесном низовом пожаре нужно двигаться перпендикулярно к направлению огня, по просекам, дорогам, берегам рек или полянам.

              При лесном верховом пожаре передвигайтесь по лесу, пригнувшись к земле и прикрыв дыхательные пути влажной тряпкой.
              
              Если у вас нет никакой возможности выйти из опасной зоны, постарайтесь отыскать в лесу какой-нибудь водоём и войдите в него.</p>
          </div>
        </div>
        <div class="row no-gutters align-items-center ftco-animate">
          <div class="col-md-6 order-md-3 mb-md-0 mb-5">
            <img src="images/bg_4.jpg" width=500 class="img-fluid">
          </div>
          <div class="col-md-6 p-md-5 order-md-1">
            <h3 class="h3 mb-4">Эвакуация</h3>
            <p>Если огонь начал подбираться к населённому пункту, необходимо принять коллективные меры по его тушению. Самая крайняя мера – немедленная эвакуация жителей этого населённого пункта. В этом случае вы должны беспрекословно слушаться работников спасательных служб. Не поддавайтесь панике и ждите оказания помощи. При невозможности забрать с собой личное имущество закопайте его в землю. Ждать помощи лучше всего на больших открытых пространствах или в специальных укрытиях. </p>
          </div>
        </div>

      </div>
    </section>

    

   
    <footer class="ftco-footer ftco-bg-dark">
    <div class="footer">
    <?php 
            if($session_user) {
              $login = mysqli_query($connect, "SELECT login FROM users WHERE id=".$session_user['id']);
              $row_login = mysqli_fetch_array($login);
              echo "Вы вошли под логином " . $row_login['login'];
              echo "<p><a href='exit.php'>Выход</a></p>";
           }
          ?>
          <p>Для создания приложения были использованы открытые данные с сайта greenpeace.ru. Отчет за 2021 год.</p>
          <p>Источник: https://greenpeace.ru/wp-content/uploads/2021/08/2021-08-20_4-ISDM.pdf</p>
          </div>
    </footer>

    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#4586ff"/></svg></div>

    
    <script src="https://code.jquery.com/jquery-3.6.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
    
  </body>
</html>