const tooltip = document.querySelector('.tooltip');
const districts = document.querySelectorAll('.district');
const regions = document.querySelectorAll('.region');
const popupBg = document.querySelector('.info_bg');
const popup = document.querySelector('.info');

districts.forEach(district => {
    district.addEventListener('click', function() {
        popup.querySelector(".info_photo").setAttribute('src', this.dataset.photo);
        popup.querySelector('.info_title').innerText = this.dataset.title;
        popup.querySelector('.info_amount').innerText = this.dataset.amount;
        popup.querySelector('.info_square').innerText = this.dataset.square;
        popupBg.classList.add('active');
    });
    district.addEventListener('mousemove', function(e) {
        tooltip.innerText = this.dataset.title;
        tooltip.style.top = (e.y + 5) + 'px';
        tooltip.style.left = (e.x + 5) + 'px';
    });

    district.addEventListener('mouseenter', function() {
        tooltip.style.display = 'block';
    });
    district.addEventListener('mouseleave', function() {
        tooltip.style.display = 'none';
    });
});

regions.forEach(region => {
    region.addEventListener('click', function() {
        popup.querySelector(".info_photo").setAttribute('src', this.dataset.photo);
        popup.querySelector('.info_title').innerText = this.dataset.title;
        popup.querySelector('.info_amount').innerText = this.dataset.amount;
        popup.querySelector('.info_square').innerText = this.dataset.square;
        popupBg.classList.add('active');
    });
    region.addEventListener('mousemove', function(e) {
        tooltip.innerText = this.dataset.title;
        tooltip.style.top = (e.y + 5) + 'px';
        tooltip.style.left = (e.x + 5) + 'px';
    });

    region.addEventListener('mouseenter', function() {
        tooltip.style.display = 'block';
    });
    region.addEventListener('mouseleave', function() {
        tooltip.style.display = 'none';
    });
});

document.addEventListener('click', (e) => {
    if(e.target === popupBg) {
        popupBg.classList.remove('active');
    }
});