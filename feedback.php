<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Wildfires - Feedback</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <link rel='stylesheet' href='css/bootstrap.min.css'>
  <link rel="stylesheet prefetch" href="css/signup_auth.css">

  
</head>

    <?php
require("connectdb.php");
require("session.php");

$row_role = mysqli_fetch_array(mysqli_query($connect, "SELECT role FROM `users` WHERE id=".$session_user['id']));
$role = $row_role['role'];

if($row_role['role'] == 'user') {
$row_name = mysqli_fetch_array(mysqli_query($connect, "SELECT name FROM `users` WHERE id=".$session_user['id']));
$name = $row_name['name'];
$row_email = mysqli_fetch_array(mysqli_query($connect, "SELECT email FROM `users` WHERE id=".$session_user['id']));
$email = $row_email['email'];

	echo "
  <body>
    <div class='container'>
    <header>
      <h1>
        <a>
          <img src='images/feedback.svg' alt='Authentic Collection'>
        </a>
      </h1>
    </header>
  <h1 class='text-center'>Обратная связь</h1>
	<form method='POST' action='feedback.php' class='registration-form'>
		<label>
			<span class='label-text'>Ваше имя:</span>
			<input type='text' name='name' value='$name'>
		</label>
		<label>
			<span class='label-text'>Ваш email:</span>
			<input type='text' name='email' value='$email'>
		</label>
        <label class='textarea'>
			<span class='label-text'>Ваше сообщение:</span>
			<textarea type='textarea' name='message' rows='5'></textarea>
        </label>
		<div class='text-center'>
			<button type='submit' class='submit' name='register'>Отправить сообщение</button>
		</div>";
	

if (!empty($_POST["message"])){
       mysqli_query($connect, "INSERT INTO feedback (name, email, message) VALUES (
            \"".$_POST["name"]."\", 
            \"".$_POST["email"]."\",
            \"".$_POST["message"]."\"
            )"
        );
        echo "<h4 style='text-align:center'>Ваше сообщение успешно отправлено!</h4>";
	header( "refresh:2; url=index.php" );

    }
    
	echo "</form>";
  echo "</div>
  </body>";
  
}
if($row_role['role'] == 'admin') {
  echo "<div class='admin'>";
  echo "<h4 style='text-align:center'>Обратная связь от пользователей сайта</h4>";
    $sql_select_feedback = "SELECT name, email, message FROM `feedback`";
  $result_feedback = mysqli_query($connect, $sql_select_feedback);
  $row_feedback = mysqli_fetch_array($result_feedback);
  echo "<p></p><table width='100%'>
   <tr>
  <th width='25%'>Имя пользователя</th>
  <th width='25%'>Email</th>
  <th width='50%'>Сообщение</th>
  </tr>";
  do
    { 
      echo "<tr>
      <td width='25%'>" .$row_feedback['name'] . "</td>
      <td width='25%'>" .$row_feedback['email'] . "</td>
      <td width='50%'>" .$row_feedback['message'] . "</td>
     </tr>";  
    }
  while($row_feedback = mysqli_fetch_array($result_feedback));
  echo "</table>";
}
echo "</div>";
?>

</html>

